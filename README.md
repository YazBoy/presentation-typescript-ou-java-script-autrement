# README #
TypeScript ou JavaScript Autrement,
### Le contenu ###
Dans ce répo vous trouvez un ensemble d'exemples de code TypeScript et la présentation de quelques concepts 
pour plus d'approfondissement veuillez consulter https://www.typescriptlang.org/
dans chaque sous projets vous allez trouver un readme dédié.

## TypeScript ou JavaScript autrement ##
### Avant de démarrer l'apprentissage ###

Installez typescript et lite-server
```
npm i typings typescript lite-server -g
```

Pour compiler TS en JS  
```
tsc –w
```
pour créer un fichier de config *tsconfig.json*
```
tsc -init
```
pour lancer lite-server
```
lite-server
```

### Pour voir les sources:
cliquer ici [Lien vers les sources ](https://bitbucket.org/YazBoy/presentation-typescript-ou-java-script-autrement/src)
### Pour la presentation 
cliquer ici [Lien vers la presentation](https://bitbucket.org/YazBoy/presentation-typescript-ou-java-script-autrement/raw/5a0479b880238c843d14b6754c7560d7430a955a/typescript/presentation_typescript.pdf)